import React, { Component } from "react";
import TicketTaskItem from "./TicketTask/TicketTaskItem";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getBacklog } from "../actions/ticketTaskActions";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import isAfter from "date-fns/isAfter";

class Ticket extends Component {
  constructor() {
    super();
    let sevenDaysFromToday = Date.now() - 7*24*60*60*1000;
    this.state = {
        priority_todo_filter_value: "",
        priority_todo_filter_startDate: new Date(sevenDaysFromToday),
        priority_todo_filter_endDate: new Date(),
        priority_in_progress_filter_value: "",
        priority_in_progress_startDate: new Date(sevenDaysFromToday),
        priority_in_progress_endDate: new Date(),
        priority_testing_filter_value: "",
        priority_testing_filter_startDate: new Date(sevenDaysFromToday),
        priority_testing_filter_endDate: new Date(),        
        priority_resolved_filter_value: "",
        priority_resolved_filter_startDate: new Date(sevenDaysFromToday),
        priority_resolved_filter_endDate: new Date()
    };
    this.handleChange = this.handleChange.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.getBacklog();
  }

  handleChange = ({ 
      priority_todo_filter_startDate,
      priority_todo_filter_endDate, 
      priority_in_progress_startDate,
      priority_in_progress_endDate,
      priority_testing_filter_startDate,
      priority_testing_filter_endDate,
      priority_resolved_filter_startDate,
      priority_resolved_filter_endDate
    }) => {
      priority_todo_filter_startDate = priority_todo_filter_startDate || this.state.priority_todo_filter_startDate;
      priority_todo_filter_endDate = priority_todo_filter_endDate || this.state.priority_todo_filter_endDate;
      priority_in_progress_startDate = priority_in_progress_startDate || this.state.priority_in_progress_startDate;
      priority_in_progress_endDate = priority_in_progress_endDate || this.state.priority_in_progress_endDate;
      priority_testing_filter_startDate = priority_testing_filter_startDate || this.state.priority_testing_filter_startDate;
      priority_testing_filter_endDate = priority_testing_filter_endDate || this.state.priority_testing_filter_endDate;
      priority_resolved_filter_startDate = priority_resolved_filter_startDate || this.state.priority_resolved_filter_startDate;
      priority_resolved_filter_endDate = priority_resolved_filter_endDate || this.state.priority_resolved_filter_endDate;

    if (isAfter(priority_todo_filter_startDate, priority_todo_filter_endDate)) {
      priority_todo_filter_endDate = priority_todo_filter_startDate;
    }
    this.setState({ priority_todo_filter_startDate, priority_todo_filter_endDate });

    if (isAfter(priority_in_progress_startDate, priority_in_progress_endDate)) {
      priority_in_progress_endDate = priority_in_progress_startDate;
    }
    this.setState({ priority_in_progress_startDate, priority_in_progress_endDate });

    if (isAfter(priority_testing_filter_startDate, priority_testing_filter_endDate)) {
      priority_testing_filter_endDate = priority_testing_filter_startDate;
    }
    this.setState({ priority_testing_filter_startDate, priority_testing_filter_endDate });

    if (isAfter(priority_resolved_filter_startDate, priority_resolved_filter_endDate)) {
      priority_resolved_filter_endDate = priority_resolved_filter_startDate;
    }
    this.setState({ priority_resolved_filter_startDate, priority_resolved_filter_endDate });
  };

  handlePriorityTodoChangeStart = priority_todo_filter_startDate => this.handleChange({ priority_todo_filter_startDate });
  handlePriorityTodoChangeEnd = priority_todo_filter_endDate => this.handleChange({ priority_todo_filter_endDate });
  handleInProgressChangeStart = priority_in_progress_startDate => this.handleChange({ priority_in_progress_startDate });
  handleInProgressChangeEnd = priority_in_progress_endDate => this.handleChange({ priority_in_progress_endDate });
  handleTestingChangeStart = priority_testing_filter_startDate => this.handleChange({ priority_testing_filter_startDate });
  handleTestingChangeEnd = priority_testing_filter_endDate => this.handleChange({ priority_testing_filter_endDate });
  handleResolvedChangeStart = priority_resolved_filter_startDate => this.handleChange({ priority_resolved_filter_startDate });
  handleResolvedChangeEnd = priority_resolved_filter_endDate => this.handleChange({ priority_resolved_filter_endDate });

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { ticket_tasks } = this.props.ticket_tasks;

    let BoardContent;
    let todoItems = [];
    let inProgressItems = [];
    let testingItems = [];
    let resolvedItems = [];

    let totalTodoItems = 0;
    let totalInProgressItems = 0;
    let totalTestingItems = 0;
    let totalResolvedItems = 0;

    const BoardAlgorithm = ticket_tasks => {
      if (ticket_tasks.length < 1) {
        return (
          <div className="alert alert-info text-center" role="alert">
            No tickets on this board
          </div>
        );
      } else {
        const tasks = ticket_tasks
          .map(ticket_task => (
            <TicketTaskItem key={ticket_task.id} ticket_task={ticket_task} />
          ));

        for (let i = 0; i < tasks.length; i++) {
          if (tasks[i].props.ticket_task.status === "todo" 
                && (
                    tasks[i].props.ticket_task.priority === this.state.priority_todo_filter_value
                    || this.state.priority_todo_filter_value === ""
                ) && (
                    Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) >= new Date(this.state.priority_todo_filter_startDate.toDateString()).getTime()  
                    && Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) <= new Date(this.state.priority_todo_filter_endDate.toDateString()).getTime()
                )
          ) {
            todoItems.push(tasks[i]);
          }
          if (tasks[i].props.ticket_task.status === "todo"){
            totalTodoItems+=1
          }
          if (tasks[i].props.ticket_task.status === "inProgress"
                && (
                    tasks[i].props.ticket_task.priority === this.state.priority_in_progress_filter_value
                    || this.state.priority_in_progress_filter_value === ""
                ) && (
                    Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) >= new Date(this.state.priority_in_progress_startDate.toDateString()).getTime()  
                    && Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) <= new Date(this.state.priority_in_progress_endDate.toDateString()).getTime()
                )
          ) {
            inProgressItems.push(tasks[i]);
          }
          if (tasks[i].props.ticket_task.status === "inProgress"){
            totalInProgressItems+=1
          }
          if (tasks[i].props.ticket_task.status === "testing"
                && (
                    tasks[i].props.ticket_task.priority === this.state.priority_testing_filter_value
                    || this.state.priority_testing_filter_value === ""
                ) && (
                  Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) >= new Date(this.state.priority_testing_filter_startDate.toDateString()).getTime()  
                  && Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) <= new Date(this.state.priority_testing_filter_endDate.toDateString()).getTime()
                )
          ) {
            testingItems.push(tasks[i]);
          }
          if (tasks[i].props.ticket_task.status === "testing"){
            totalTestingItems+=1
          }
          if (tasks[i].props.ticket_task.status === "resolved"
                && (
                    tasks[i].props.ticket_task.priority === this.state.priority_resolved_filter_value
                    || this.state.priority_resolved_filter_value === ""
                ) && (
                  Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) >= new Date(this.state.priority_resolved_filter_startDate.toDateString()).getTime()  
                  && Date.parse(new Date(tasks[i].props.ticket_task.createdAt).toDateString()) <= new Date(this.state.priority_resolved_filter_endDate.toDateString()).getTime()
                )
          ) {
            resolvedItems.push(tasks[i]);
          }
          if (tasks[i].props.ticket_task.status === "resolved"){
            totalResolvedItems+=1
          }
        }
        
        const filteredTodoItems = todoItems.length
        const filteredInProgressItems = inProgressItems.length
        const filteredTestingItems = testingItems.length
        const filteredResolvedItems = resolvedItems.length

        return (
          <React.Fragment>
            <div className="container-fluid">
              <div className="row border-between">
                <div className="col-md-3">
                  <div className="card text-center mb-2">
                    <div className="card-header bg-info text-white">
                      <h3>To Do ({ filteredTodoItems } of { totalTodoItems })</h3>
                      <select
                        className="form-control form-control-xs priority-filter"
                        name="priority_todo_filter_value"
                        onChange={this.onChange}
                        value={this.state.priority_todo_filter_value} >
                            <option value="">Select priority</option>
                            <option value="unprioritized">UNPRIORITIZED</option>
                            <option value="low">LOW</option>
                            <option value="medium">MEDIUM</option>
                            <option value="high">HIGH</option>
                            <option value="critical">CRITICAL</option>
                      </select>
                      <div className="column">
                        <DatePicker
                            selected={this.state.priority_todo_filter_startDate}
                            selectsStart
                            startDate={this.state.priority_todo_filter_startDate}
                            endDate={this.state.priority_todo_filter_endDate}
                            onChange={this.handlePriorityTodoChangeStart}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                        <DatePicker
                            selected={this.state.priority_todo_filter_endDate}
                            selectsEnd
                            startDate={this.state.priority_todo_filter_startDate}
                            endDate={this.state.priority_todo_filter_endDate}
                            onChange={this.handlePriorityTodoChangeEnd}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                      </div>                      
                    </div>
                  </div>
                  {todoItems}
                </div>
                <div className="col-md-3">
                  <div className="card text-center mb-2">
                    <div className="card-header bg-info text-white">
                      <h3>In Progress ({ filteredInProgressItems } of { totalInProgressItems })</h3>
                      <select
                        className="form-control form-control-xs priority-filter"
                        name="priority_in_progress_filter_value"
                        onChange={this.onChange}
                        value={this.state.priority_in_progress_filter_value} >
                            <option value="">Select priority</option>
                            <option value="unprioritized">UNPRIORITIZED</option>
                            <option value="low">LOW</option>
                            <option value="medium">MEDIUM</option>
                            <option value="high">HIGH</option>
                            <option value="critical">CRITICAL</option>
                      </select>
                      <div className="column">
                        <DatePicker
                            selected={this.state.priority_in_progress_startDate}
                            selectsStart
                            startDate={this.state.priority_in_progress_startDate}
                            endDate={this.state.priority_in_progress_endDate}
                            onChange={this.handleInProgressChangeStart}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                        <DatePicker
                            selected={this.state.priority_in_progress_endDate}
                            selectsEnd
                            startDate={this.state.priority_in_progress_startDate}
                            endDate={this.state.priority_in_progress_endDate}
                            onChange={this.handleInProgressChangeEnd}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                        </div>
                    </div>
                  </div>
                  {inProgressItems}
                </div>
                <div className="col-md-3">
                  <div className="card text-center mb-2">
                    <div className="card-header bg-info text-white">
                      <h3>Testing ({ filteredTestingItems } of { totalTestingItems })</h3>
                      <select
                        className="form-control form-control-xs priority-filter"
                        name="priority_testing_filter_value"
                        onChange={this.onChange}
                        value={this.state.priority_testing_filter_value} >
                            <option value="">Select priority</option>
                            <option value="unprioritized">UNPRIORITIZED</option>
                            <option value="low">LOW</option>
                            <option value="medium">MEDIUM</option>
                            <option value="high">HIGH</option>
                            <option value="critical">CRITICAL</option>
                      </select>
                      <div className="column">
                        <DatePicker
                            selected={this.state.priority_testing_filter_startDate}
                            selectsStart
                            startDate={this.state.priority_testing_filter_startDate}
                            endDate={this.state.priority_testing_filter_endDate}
                            onChange={this.handleTestingChangeStart}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                        <DatePicker
                            selected={this.state.priority_testing_filter_endDate}
                            selectsEnd
                            startDate={this.state.priority_testing_filter_startDate}
                            endDate={this.state.priority_testing_filter_endDate}
                            onChange={this.handleTestingChangeEnd}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                        </div>
                    </div>
                  </div>
                  {testingItems}
                </div>
                <div className="col-md-3">
                  <div className="card text-center mb-2">
                    <div className="card-header bg-info text-white">
                      <h3>Resolved ({ filteredResolvedItems } of { totalResolvedItems })</h3>
                      <select
                        className="form-control form-control-xs priority-filter"
                        name="priority_resolved_filter_value"
                        onChange={this.onChange}
                        value={this.state.priority_resolved_filter_value} >
                            <option value="">Select priority</option>
                            <option value="unprioritized">UNPRIORITIZED</option>
                            <option value="low">LOW</option>
                            <option value="medium">MEDIUM</option>
                            <option value="high">HIGH</option>
                            <option value="critical">CRITICAL</option>
                      </select>
                      <div className="column">
                        <DatePicker
                            selected={this.state.priority_resolved_filter_startDate}
                            selectsStart
                            startDate={this.state.priority_resolved_filter_startDate}
                            endDate={this.state.priority_resolved_filter_endDate}
                            onChange={this.handleResolvedChangeStart}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                        <DatePicker
                            selected={this.state.priority_resolved_filter_endDate}
                            selectsEnd
                            startDate={this.state.priority_resolved_filter_startDate}
                            endDate={this.state.priority_resolved_filter_endDate}
                            onChange={this.handleResolvedChangeEnd}
                            dateFormat="yyyy-MM-dd"
                            className="form-control form-control-xs"
                        />
                        </div>
                    </div>
                  </div>
                  {resolvedItems}
                </div>
              </div>
            </div>
          </React.Fragment>
        );
      }
    };

    BoardContent = BoardAlgorithm(ticket_tasks);

    return (
      <div className="container-fluid">
        <hr />
        {BoardContent}
      </div>
    );
  }
}

Ticket.propTypes = {
  getBacklog: PropTypes.func.isRequired,
  ticket_tasks: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  ticket_tasks: state.ticket_task
});

export default connect(
  mapStateToProps,
  { getBacklog }
)(Ticket);
