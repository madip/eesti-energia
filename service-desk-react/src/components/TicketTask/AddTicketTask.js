import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addTicketTask } from "../../actions/ticketTaskActions";
import classnames from "classnames";

class AddTicketTask extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      email: "",
      description: "",
      priority: "",
      status: "",
      createdAt: "",
      updatedAt: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const newTicketTask = {
      title: this.state.title,
      email: this.state.email,
      description: this.state.description,
      priority: this.state.priority,
      status: this.state.status,
      createdAt: this.state.createdAt,
      updatedAt: this.state.updatedAt
    };
    this.props.addTicketTask(newTicketTask, this.props.history);
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="addTicketTask">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/" className="btn btn-secondary">
                Back to Board
              </Link>
              <h4 className="display-4 text-center">Add Ticket</h4>
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.title
                    })}
                    name="title"
                    value={this.state.title}
                    onChange={this.onChange}
                    placeholder="Title"
                  />
                  {errors.title && (
                    <div className="invalid-feedback">{errors.title}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.email
                    })}
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                    placeholder="Email"
                  />
                  {errors.email && (
                    <div className="invalid-feedback">{errors.email}</div>
                  )}
                </div>
                <div className="form-group">
                  <textarea
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.description
                    })}
                    placeholder="description"
                    value={this.state.description}
                    onChange={this.onChange}
                    name="description"
                  />
                  {errors.description && (
                    <div className="invalid-feedback">{errors.description}</div>
                  )}
                </div>
                <div className="form-group">
                  <select
                    className="form-control form-control-lg"
                    name="priority"
                    value={this.state.priority}
                    onChange={this.onChange}
                  >
                    <option value="">Select priority</option>
                    <option value="unprioritized">UNPRIORITIZED</option>
                    <option value="low">LOW</option>
                    <option value="medium">MEDIUM</option>
                    <option value="high">HIGH</option>
                    <option value="critical">CRITICAL</option>
                  </select>
                </div>
                <div className="form-group">
                  <select
                    className="form-control form-control-lg"
                    name="status"
                    value={this.state.status}
                    onChange={this.onChange}
                  >
                    <option value="">Select Status</option>
                    <option value="todo">TO DO</option>
                    <option value="inProgress">IN PROGRESS</option>
                    <option value="testing">TESTING</option>
                    <option value="resolved">RESOLVED</option>
                  </select>
                </div>
                <input
                  type="submit"
                  className="btn btn-service-desk-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddTicketTask.propTypes = {
  addTicketTask: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { addTicketTask }
)(AddTicketTask);
