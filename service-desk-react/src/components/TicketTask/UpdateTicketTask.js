import React, { Component } from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { getTicketTask, addTicketTask } from "../../actions/ticketTaskActions";

class UpdateTicketTask extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      title: "",
      email: "",
      description: "",
      priority: "",
      status: "",
      createdAt: "",
      updatedAt: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    const {
      id,
      title,
      email,
      description,
      priority,
      status,
      createdAt,
      updatedAt
    } = nextProps.ticket_task;

    this.setState({
      id,
      title,
      email,
      description,
      priority,
      status,
      createdAt,
      updatedAt
    });
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getTicketTask(id);
  }

  onSubmit(e) {
    e.preventDefault();
    const updateTask = {
      id: this.state.id,
      title: this.state.title,
      email: this.state.email,
      description: this.state.description,
      priority: this.state.priority,
      status: this.state.status,
      createdAt: this.state.createdAt,
      updatedAt: this.state.updatedAt
    };
    this.props.addTicketTask(updateTask, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="updateTicketTask">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/" className="btn btn-secondary">
                Back to Board
              </Link>
              <h4 className="display-4 text-center">Update Ticket</h4>
              <p>
                Created: {this.state.createdAt}
                <br />
                Updated: {this.state.updatedAt}
              </p>
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.title
                    })}
                    name="title"
                    value={this.state.title}
                    onChange={this.onChange}
                    placeholder="Title"
                  />
                  {errors.title && (
                    <div className="invalid-feedback">{errors.title}</div>
                  )}
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.email
                    })}
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                    placeholder="Email"
                  />
                  {errors.email && (
                    <div className="invalid-feedback">{errors.email}</div>
                  )}
                </div>
                <div className="form-group">
                  <textarea
                    className={classnames("form-control form-control-lg", {
                      "is-invalid": errors.description
                    })}
                    placeholder="description"
                    value={this.state.description}
                    onChange={this.onChange}
                    name="description"
                  />
                  {errors.description && (
                    <div className="invalid-feedback">{errors.description}</div>
                  )}
                </div>
                <div className="form-group">
                  <select
                    className="form-control form-control-lg"
                    name="priority"
                    value={this.state.priority}
                    onChange={this.onChange}
                  >
                    <option value="">Select priority</option>
                    <option value="unprioritized">UNPRIORITIZED</option>
                    <option value="low">LOW</option>
                    <option value="medium">MEDIUM</option>
                    <option value="high">HIGH</option>
                    <option value="critical">CRITICAL</option>
                  </select>
                </div>
                <div className="form-group">
                  <select
                    className="form-control form-control-lg"
                    name="status"
                    value={this.state.status}
                    onChange={this.onChange}
                  >
                    <option value="">Select Status</option>
                    <option value="todo">TO DO</option>
                    <option value="inProgress">IN PROGRESS</option>
                    <option value="testing">TESTING</option>
                    <option value="resolved">RESOLVED</option>
                  </select>
                </div>
                <input
                  type="submit"
                  className="btn btn-service-desk-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateTicketTask.propTypes = {
  ticket_task: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  getTicketTask: PropTypes.func.isRequired,
  addTicketTask: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ticket_task: state.ticket_task.ticket_task,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { getTicketTask, addTicketTask }
)(UpdateTicketTask);
