import React, { Component } from 'react'
import { Link } from "react-router-dom";
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { closeTicketTask } from "../../actions/ticketTaskActions";

class TicketTaskItem extends Component {

  onCloseTicketClick(id){
    this.props.closeTicketTask(id);
  }

  render() {
    const { ticket_task } = this.props;
    return (
        <div className="card mb-1 bg-light">

            <div className="card-header">
                <span className="text-primary">ID: {ticket_task.id}</span>
                <span> - </span>
                <span className="card-title"><b>{ticket_task.title}</b></span>
            </div>
            <div className="card-body bg-light">
                <span>
                    Created: {ticket_task.createdAt}
                </span>
                <p>
                    Priority: 
                    <span className={classnames("", {                    
                        "text-secondary": ticket_task.priority==="low",
                        "text-warning": ticket_task.priority==="medium",
                        "text-danger": ticket_task.priority==="high",
                        "bg-danger text-white": ticket_task.priority==="critical"
                      })}>
                      {ticket_task.priority}
                      </span>
                </p>
                <p className="card-text text-truncate ">
                    {ticket_task.description}
                </p>
                <Link to={`updateTicketTask/${ticket_task.id}`} className="btn btn-primary">
                    View
                </Link>
                <button className={classnames("btn btn-danger ml-4 d-none", {
                    "d-inline": ticket_task.status==="resolved"
                  })}
                  onClick={this.onCloseTicketClick.bind(this, ticket_task.id)}
                >
                    Close ticket
                </button>
            </div>
        </div>
    )
  }
}

TicketTaskItem.propTypes = {
    closeTicketTask: PropTypes.func.isRequired
};

export default connect(
    null,
    { closeTicketTask }
) (TicketTaskItem);