import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar';
import Ticket from './components/Ticket';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import AddTicketTask from './components/TicketTask/AddTicketTask';
import { Provider } from "react-redux";
import store from "./store";
import UpdateTicketTask from './components/TicketTask/UpdateTicketTask';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Navbar />
            <Route exact path="/" component={Ticket}/>
            <Route exact path="/addTicketTask" component={AddTicketTask}/>
            <Route exact path="/updateTicketTask/:id" component={UpdateTicketTask}/>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
