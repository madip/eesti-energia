export const GET_ERRORS = "GET_ERRORS";
export const GET_TICKETS = "GET_TICKETS";
export const GET_TICKET = "GET_TICKET";
export const CLOSE_TICKET = "CLOSE_TICKET";