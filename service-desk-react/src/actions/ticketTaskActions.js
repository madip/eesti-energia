import axios from "axios";
import {GET_ERRORS, GET_TICKETS, GET_TICKET, CLOSE_TICKET} from "./types";

export const addTicketTask = (ticket, history) => async dispatch => {
    try {
        await axios.post("http://localhost:8080/api/service-desk/ticket", ticket);
        history.push("/");
        dispatch({
            type: GET_ERRORS,
            payload: {}
        });
    } catch (error) {
        dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        });
    }
};

export const getBacklog = () => async dispatch => {
    const res = await axios.get("http://localhost:8080/api/service-desk/tickets")
    dispatch({
        type: GET_TICKETS,
        payload: res.data
    })
};

export const getTicketTask = (id, history) => async dispatch => {
    try {
      const res = await axios.get(`http://localhost:8080/api/service-desk/ticket/${id}`);
      dispatch({
        type: GET_TICKET,
        payload: res.data
      });
    } catch (error) {
      history.push("/");
    }
};

export const closeTicketTask = id => async dispatch => {
  if(window.confirm(`You are closing the ticket with ID: ${id}, this action cannot be undone`)) {
      await axios.post(`http://localhost:8080/api/service-desk/ticket/close/${id}`);
      dispatch({
          type: CLOSE_TICKET,
          payload: id
      });
  }    
};