import { GET_TICKETS, GET_TICKET, CLOSE_TICKET } from "../actions/types";

const initialState = {
    ticket_tasks: [],
    ticket_task: {}
}

export default function(state=initialState, action) {
    switch(action.type){

        case GET_TICKETS:
            return {
                ...state,
                ticket_tasks: action.payload
            };
        case GET_TICKET:
            return {
                ...state,
                ticket_task: action.payload
            };
        case CLOSE_TICKET:
            return {
                ...state,
                ticket_tasks: state.ticket_tasks.filter(
                    ticket_task => ticket_task.id !== action.payload
                )
            };
        default:
            return state;
    }
}