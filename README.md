# Serivce Desk rakendus #

![Screenshot](screenshot_of_running_application.png)

Projekt on loodud Eesti Energia testülesande raames.
Projekt töötab kahes osas. Back-end on java rakendus ning front-end on reacti rakendus.
Ülesande kirjeldus asub failis nimega "Testülesanne_ServiceDesk.docx".

### Küsimused, mis tekkisid enne arenduse alustamist ###

Antud küsimusi ei küsinud ma Eesti Energialt testülesande raames, kuna soovisin ülesannet kiiremini ära sooritada. Igapäeva arenduses küsin ma ikkagi küsimused enne kui arendus hakkab.

* Mis süsteemi jaoks service deski tarvis on? Mis äriga on tegemist?
* Millised on viieastmelise prioriteerimise süsteemide nimed?
* Millised pileti omadused on nõutud (required)?
* Kas prioriteerimissüsteem nõuded on fikseeritud või peab saama neid juurde lisada läbi service desk süsteemi?
* Piletite sorteerimine on kuupäeva alusel, millist kuupäeva on mõeldud, pileti loomise kuupäeva, pileti valmis saamise kuupäeva või mingi muu kuupäeva valik?