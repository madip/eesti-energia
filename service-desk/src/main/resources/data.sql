INSERT INTO TICKET (title, email, description, priority, status, created_at, is_closed) VALUES
('Broken toilet', 'madispeep@gmail.com', 'Toilet needs fixing. Some of the blue stuff is leaking', 1, 0, CURRENT_DATE-2, FALSE),
('Repair tv in room 301', 'madispeep@gmail.com', 'TV shows only one channel (TTV) and customers are complaining', 2, 0, CURRENT_DATE-1, FALSE),
('Buy more TP', 'madispeep@gmail.com', 'We need more toilet paper for the hotel rooms', 2, 0, CURRENT_DATE-5, FALSE),
('Fix the web', 'madispeep@gmail.com', 'Our web page has no customers due to login problems', 1, 1, CURRENT_DATE-4, FALSE),
('Buy more cats', 'madispeep@gmail.com', 'We need more of them! Actually we do not. We need more dogs!', 4, 2, CURRENT_DATE-7, FALSE),
('Pancakes', 'madispeep@gmail.com', 'Buy a pancake making machine and serve some pancakes', 1, 3, CURRENT_DATE-2, FALSE),
('Parking spaces', '', 'Create more parking spaces for customers', 2, 3, CURRENT_DATE-1, FALSE);