package com.peepme.servicedesk.web;

import com.peepme.servicedesk.domain.Status;
import com.peepme.servicedesk.domain.Ticket;
import com.peepme.servicedesk.service.TicketService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/service-desk")
@CrossOrigin
@NoArgsConstructor
public class TicketController {

    @Autowired
    private TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PostMapping("/ticket")
    public ResponseEntity<?> addOrUpdateTicket(@Valid @RequestBody Ticket ticket, BindingResult result) {

        if (result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for (FieldError error : result.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
        }
        ticket.setIsClosed(false);
        Ticket newTicket = ticketService.addOrUpdate(ticket);
        return new ResponseEntity<>(newTicket, HttpStatus.OK);
    }

    @GetMapping("/all-tickets")
    public Iterable<Ticket> getAllTickets() {
        return ticketService.findAll();
    }

    @GetMapping("/tickets")
    public Iterable<Ticket> getNotClosedTickets() {
        return ticketService.findTicketsByIsClosedFalse();
    }

    @GetMapping("/ticket/{id}")
    public ResponseEntity<?> getTicketById(@PathVariable Long id) {
        Ticket ticket = ticketService.findById(id);
        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

    @PostMapping("/ticket/close/{id}")
    public ResponseEntity<?> closeTicket(@PathVariable Long id) {

        Ticket ticket = ticketService.findById(id);
        if (!ticket.getStatus().equals(Status.RESOLVED)) {
            return new ResponseEntity<>("Closing ticket failed, because ticket status is not " + Status.RESOLVED, HttpStatus.BAD_REQUEST);
        }
        ticket.setIsClosed(true);
        ticketService.addOrUpdate(ticket);
        return new ResponseEntity<>("Closed", HttpStatus.OK);
    }
}
