package com.peepme.servicedesk.service;

import com.peepme.servicedesk.domain.Priority;
import com.peepme.servicedesk.domain.Status;
import com.peepme.servicedesk.domain.Ticket;
import com.peepme.servicedesk.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public Ticket addOrUpdate(Ticket ticket) {

        if (ticket.getPriority() == null || ticket.getPriority().equals("")) {
            ticket.setPriority(Priority.UNPRIORITIZED);
        }

        if (ticket.getStatus() == null || ticket.getStatus().equals("")) {
            ticket.setStatus(Status.TODO);
        }

        if (ticket.getCreatedAt() == null || ticket.getCreatedAt().equals("")) {
            ticket.setCreatedAt(LocalDate.now());
        } else {
            ticket.setUpdatedAt(LocalDate.now());
        }

        return ticketRepository.save(ticket);
    }

    public Iterable<Ticket> findAll() {
        return ticketRepository.findAll();
    }

    public List<Ticket> findTicketsByIsClosedFalse() {
        return ticketRepository.findTicketsByIsClosedFalse();
    }

    public Ticket findById(Long id) {
        return ticketRepository.getById(id);
    }
}
