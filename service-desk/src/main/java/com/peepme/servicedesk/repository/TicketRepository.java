package com.peepme.servicedesk.repository;

import com.peepme.servicedesk.domain.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Long> {

    Ticket getById(Long id);

    List<Ticket> findTicketsByIsClosedFalse();

    Ticket findByTitle(String title);
}
