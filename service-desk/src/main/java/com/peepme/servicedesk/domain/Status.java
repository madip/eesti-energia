package com.peepme.servicedesk.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Status {

    @JsonProperty("todo")
    TODO("todo"),
    @JsonProperty("inProgress")
    IN_PROGRESS("inProgress"),
    @JsonProperty("testing")
    TESTING("testing"),
    @JsonProperty("resolved")
    RESOLVED("resolved");

    private static Map<String, Status> FORMAT_MAP = Stream
            .of(Status.values())
            .collect(Collectors.toMap(s -> s.formatted, Function.identity()));

    private final String formatted;

    Status(String formatted) {
        this.formatted = formatted;
    }

    @JsonCreator
    public static Status fromString(String string) {
        return Optional
                .ofNullable(FORMAT_MAP.get(string))
                .orElse(Status.TODO);
    }
}
