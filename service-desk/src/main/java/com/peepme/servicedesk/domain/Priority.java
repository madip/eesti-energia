package com.peepme.servicedesk.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Priority {

    @JsonProperty("unprioritized")
    UNPRIORITIZED("unprioritized"),
    @JsonProperty("low")
    LOW("low"),
    @JsonProperty("medium")
    MEDIUM("medium"),
    @JsonProperty("high")
    HIGH("high"),
    @JsonProperty("critical")
    CRITICAL("critical");

    private static Map<String, Priority> FORMAT_MAP = Stream
            .of(Priority.values())
            .collect(Collectors.toMap(s -> s.formatted, Function.identity()));

    private final String formatted;

    Priority(String formatted) {
        this.formatted = formatted;
    }

    @JsonCreator
    public static Priority fromString(String string) {
        return Optional
                .ofNullable(FORMAT_MAP.get(string))
                .orElse(Priority.UNPRIORITIZED);
    }
}
