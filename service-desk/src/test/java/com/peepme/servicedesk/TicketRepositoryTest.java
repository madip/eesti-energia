package com.peepme.servicedesk;

import com.peepme.servicedesk.domain.Ticket;
import com.peepme.servicedesk.repository.TicketRepository;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TicketRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TicketRepository ticketRepository;

    @Before
    public void setUp() {
        this.ticketRepository.deleteAll();
    }

    @After
    public void tearDown() {
        this.ticketRepository.deleteAll();
    }

    public Ticket newTicket() {
        Ticket newTicket = new Ticket();
        newTicket.setTitle("Test title");
        newTicket.setDescription("Super description");
        return newTicket;
    }

    public List<Ticket> newTickets() {
        List<Ticket> newTickets = new ArrayList<>();
        newTickets.add(new Ticket((long) 1, "Not Closed Ticket", null, "Test description", null, null, null, null, false));
        newTickets.add(new Ticket((long) 2, "Not Closed Ticket", null, "Test description", null, null, null, null, false));
        newTickets.add(new Ticket((long) 3, "Closed Ticket", null, "Test description", null, null, null, null, true));
        newTickets.add(new Ticket((long) 4, "Not Closed Ticket", null, "Test description", null, null, null, null, false));
        return newTickets;
    }

    @Test
    public void getByTitle_returnsTicketByTitle() {
        Ticket newTicket = newTicket();

        entityManager.persistAndFlush(newTicket);
        Ticket ticket = ticketRepository.findByTitle(newTicket.getTitle());

        Assertions.assertThat(ticket.getTitle()).isEqualTo(newTicket.getTitle());
    }

    @Test
    public void getById_returnsTicketByTicketId() {
        Ticket newTicket = newTicket();

        Long newTicketId = (Long) entityManager.persistAndGetId(newTicket);
        Ticket ticket = ticketRepository.getById(newTicketId);

        Assertions.assertThat(ticket.getTitle()).isEqualTo(newTicket.getTitle());
    }

    @Test
    public void findNotClosedTickets_returnsTicketsThatAreNotClosed() {
        List<Ticket> newTickets = newTickets();

        for (Ticket newTicket : newTickets) {
            entityManager.merge(newTicket);
        }

        List<Ticket> tickets = ticketRepository.findTicketsByIsClosedFalse();
        Assertions.assertThat(tickets.size()).isEqualTo(3);
    }
}
