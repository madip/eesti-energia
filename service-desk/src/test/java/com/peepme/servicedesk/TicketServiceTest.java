package com.peepme.servicedesk;

import com.peepme.servicedesk.domain.Priority;
import com.peepme.servicedesk.domain.Status;
import com.peepme.servicedesk.domain.Ticket;
import com.peepme.servicedesk.repository.TicketRepository;
import com.peepme.servicedesk.service.TicketService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceTest {

    @Mock
    private TicketRepository ticketRepository;

    @Autowired
    private TicketService ticketService;

    @Before
    public void SetUp() {
        ticketService = new TicketService(ticketRepository);
        ticketRepository.deleteAll();
    }

    public Ticket newTicket() {
        Ticket newTicket = new Ticket();
        newTicket.setTitle("Test add ticket title");
        newTicket.setDescription("Test add description");
        return newTicket;
    }

    public List<Ticket> newTicketList() {
        List<Ticket> newTickets = new ArrayList<>();
        newTickets.add(new Ticket((long) 1, "Test title 1", "madispeep@gmail.com", "Test description", Priority.LOW, Status.TODO, LocalDate.now(), null, false));
        newTickets.add(new Ticket((long) 2, "Test title 2", "madispeep@gmail.com", "Test description", Priority.HIGH, Status.IN_PROGRESS, LocalDate.now(), null, false));
        newTickets.add(new Ticket((long) 3, "Test title 3", "madispeep@gmail.com", "Test description", Priority.CRITICAL, Status.TESTING, LocalDate.now(), null, true));
        return newTickets;
    }

    @Test
    public void addOrUpdate_savesNewTicket() {
        Ticket newTicket = newTicket();

        when(ticketRepository.save(newTicket)).thenReturn(newTicket);
        Ticket ticket = ticketService.addOrUpdate(newTicket);

        assertEquals(newTicket, ticket);
        assertThat(newTicket.getStatus()).isEqualTo(ticket.getStatus());
        assertThat(newTicket.getPriority()).isEqualTo(ticket.getPriority());
        assertThat(newTicket.getCreatedAt()).isEqualTo(ticket.getCreatedAt());
        assertThat(newTicket.getUpdatedAt()).isNull();
    }

    @Test
    public void findAll_returnsAllTickets() {
        List<Ticket> newTickets = newTicketList();

        when(ticketRepository.findAll()).thenReturn(newTickets);
        List<Ticket> tickets = (List<Ticket>) ticketService.findAll();

        assertEquals(newTickets, tickets);
        assertThat(newTickets.get(0).getTitle()).isEqualTo(tickets.get(0).getTitle());
        assertThat(newTickets.get(1).getStatus()).isEqualTo(tickets.get(1).getStatus());
        assertThat(newTickets.size()).isEqualTo(3);
    }

    @Test
    public void findTicketsByIsClosedFalse_returnsAllOpenTickets() {
        List<Ticket> newTickets = newTicketList();

        when(ticketRepository.findTicketsByIsClosedFalse()).thenReturn(newTickets);
        List<Ticket> tickets = ticketService.findTicketsByIsClosedFalse();

        assertEquals(newTickets, tickets);
        assertThat(newTickets.get(0).getTitle()).isEqualTo(tickets.get(0).getTitle());
        assertThat(newTickets.get(1).getStatus()).isEqualTo(tickets.get(1).getStatus());
        assertThat(newTickets.size()).isEqualTo(tickets.size());
    }

    @Test
    public void findById_returnsTicketById() {
        Ticket newTicket = newTicket();

        when(ticketRepository.getById((long) 1)).thenReturn(newTicket);
        Ticket ticket = ticketService.findById((long) 1);

        assertEquals(newTicket, ticket);
        assertThat(newTicket.getStatus()).isEqualTo(ticket.getStatus());
        assertThat(newTicket.getPriority()).isEqualTo(ticket.getPriority());
        assertThat(newTicket.getCreatedAt()).isEqualTo(ticket.getCreatedAt());
        assertThat(newTicket.getUpdatedAt()).isNull();
    }
}
