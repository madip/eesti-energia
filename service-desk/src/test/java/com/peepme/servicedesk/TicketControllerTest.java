package com.peepme.servicedesk;

import com.peepme.servicedesk.domain.Priority;
import com.peepme.servicedesk.domain.Status;
import com.peepme.servicedesk.domain.Ticket;
import com.peepme.servicedesk.service.TicketService;
import com.peepme.servicedesk.web.TicketController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TicketController.class)
public class TicketControllerTest {

    @MockBean
    private TicketService ticketService;

    @Autowired
    private MockMvc mockMvc;

    private String ticketUrl = "http://localhost:8080/api/service-desk";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void reset_mocks() {
        Mockito.reset(ticketService);
    }

    private Ticket newTicket() {
        Ticket newTicket = new Ticket();
        newTicket.setId((long) 1);
        newTicket.setTitle("Test add ticket title");
        newTicket.setDescription("Test add description");
        newTicket.setStatus(Status.RESOLVED);
        newTicket.setIsClosed(false);
        return newTicket;
    }

    private static String createTicketInJson(String title, String description) {
        return "{ \"title\": \"" + title + "\", " +
                "\"description\":\"" + description + "\"}";
    }

    private List<Ticket> newTickets() {
        List<Ticket> newTickets = new ArrayList<>();
        newTickets.add(new Ticket((long) 1, "Test title 1", "madispeep@gmail.com", "Test description", Priority.LOW, Status.TODO, LocalDate.now(), null, false));
        newTickets.add(new Ticket((long) 2, "Test title 2", "madispeep@gmail.com", "Test description", Priority.HIGH, Status.IN_PROGRESS, LocalDate.now(), null, false));
        newTickets.add(new Ticket((long) 3, "Test title 3", "madispeep@gmail.com", "Test description", Priority.CRITICAL, Status.TESTING, LocalDate.now(), null, true));
        return newTickets;
    }

    @Test
    public void getAllTickets_returnsAllTickets() throws Exception {

        List<Ticket> allTickets = newTickets();

        given(ticketService.findAll()).willReturn(allTickets);

        mockMvc.perform(get(ticketUrl + "/all-tickets").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].description", is(allTickets.get(0).getDescription())))
                .andExpect(jsonPath("$[1].email", is(allTickets.get(1).getEmail())))
                .andExpect(jsonPath("$[2].isClosed", is(allTickets.get(2).getIsClosed())));
    }

    @Test
    public void getNotClosedTickets_returnAllOpenTickets() throws Exception {
        List<Ticket> allTickets = newTickets();
        allTickets.remove(2);

        given(ticketService.findTicketsByIsClosedFalse()).willReturn(allTickets);

        mockMvc.perform(get(ticketUrl + "/tickets").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].description", is(allTickets.get(0).getDescription())))
                .andExpect(jsonPath("$[1].email", is(allTickets.get(1).getEmail())))
                .andExpect(jsonPath("$[2]").doesNotExist());
    }

    @Test
    public void getTicketById_returnTicketByTicketId() throws Exception {
        Ticket newTicket = newTicket();

        given(ticketService.findById(newTicket.getId())).willReturn(newTicket);

        mockMvc.perform(get(ticketUrl + "/ticket/" + String.valueOf(newTicket.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(newTicket.getTitle())))
                .andExpect(jsonPath("$.description", is(newTicket.getDescription())));
    }

    @Test
    public void closeTicket_closeOpenTicket() throws Exception {
        Ticket newTicket = newTicket();

        given(ticketService.findById(newTicket.getId())).willReturn(newTicket);

        mockMvc.perform(
                post(ticketUrl + "/ticket/close/" + String.valueOf(newTicket.getId()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk());
    }

    @Test
    public void addOrUpdateTicket_addOrUpdatesTicket() throws Exception {
        Ticket newTicket = newTicket();

        given(ticketService.addOrUpdate(newTicket)).willReturn(newTicket);

        mockMvc.perform(
                post(ticketUrl + "/ticket")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(createTicketInJson(newTicket.getTitle(), newTicket.getDescription()))
        )
                .andExpect(status().isOk())
                .andReturn();
    }

}
